import sys
sys.path.insert(0,"src")

from polinom import Vvod
from polinom import Calculation

def test_calc_zero():
    C = Calculation()
    src = [0,2,3]
    assert C.raschet(src) == 504

def test_calc_none():
    C = Calculation()
    src = []
    assert C.raschet(src) == 505
        
    
def test_vvod_character():
    V = Vvod()
    poly = '1,2,3,a'
    assert V.parser(poly) == 404

