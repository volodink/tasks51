import re

class Vvod:
	def parser(self, poly):
		proverka = re.findall(r'[^,\d\W]\D*', poly)
		if proverka != []:
			print(proverka)
			print('Можно использовать только числа и знак запятой, замените эти аргументы')
			return 404
		lst = []
		lst = poly.split(',')
		return lst	

class Calculation:
	def raschet(self, lst):
	    if lst == 404:
	        return 404
	    if lst == []:
	        print('Не переданы значения')
	        return 505
	    numbers = (list(map(float, lst)))
	    porog = len(numbers)
	    i = 0
	    x = 0
	    while(i < porog):
	        if numbers[i] == 0:
	            print('В расчёте есть ноль который недопустим')
	            return 504
	        x = x + (1/(numbers[i]*3))
	        i = i + 1
	        return x
